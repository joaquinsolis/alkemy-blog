import styles from '../styles/Home.module.css'
import Link from 'next/link'

const Navbar = () => {
    return (
        <div className="max-w-2xl w-full flex justify-between align-baseline pt-5 pb-5">


            <Link href="/"><h1 className="text-red-500 text-2xl md:text-4xl cursor-pointer pl-2">Alkemy Blog</h1></Link>

            <nav className="flex justify-center">
                <Link href="/" ><p className="p-2 cursor-pointer">Home</p></Link>
                <Link href="/create" ><p className="p-2 cursor-pointer">Create</p></Link>
            </nav >
        </div>

    )
}

export default Navbar;