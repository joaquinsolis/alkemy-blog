import styles from '../styles/Home.module.css'

const Navbar = () => {
    return (
        <footer className={styles.footer}>
            <a
                href="https://bitbucket.org/joaquinsolis/alkemy-blog/src/master/"
                target="_blank"
                rel="noopener noreferrer"
            >
                Github
        </a>
        </footer>
    )
}

export default Navbar;