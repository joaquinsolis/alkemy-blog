# Alkemy blog (Frontend)

Challenge frontend

## Technologies 

* React
* NextJS
* Tailwind

## Install dependencies

```
npm install
```

## Run project

```
npm run dev
```