import Head from 'next/head'
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';

import styles from '../../styles/Home.module.css'
import { useRouter } from 'next/router'

export default function Home({ post }) {


    return (
        <div className={styles.container}>
            <Head>
                <title>Alkemy Blog</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar />
            <main className={styles.main}>


                <div className="flex flex-col justify-center max-w-2xl">
                    <div className="">
                        <h2 className="text-2xl font-bold">{post.title}</h2>
                        <p className="pt-4 pb-4">{post.body}</p>
                    </div>
                </div>

            </main>

            <Footer />
        </div>
    )
}
export const getStaticPaths = async () => {

    return {
        paths: [], //indicates that no page needs be created at build time
        fallback: 'blocking' //indicates the type of fallback
    }
}
// This also gets called at build time
export async function getStaticProps({ params }) {
    // params contains the post `id`.
    // If the route is like /posts/1, then params.id is 1
    const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${params.id}`)
    const post = await res.json()

    // Pass post data to the page via props
    return { props: { post } }
}