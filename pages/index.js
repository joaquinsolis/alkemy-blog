import Head from 'next/head'
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import { useState } from 'react'
import styles from '../styles/Home.module.css'
import Link from 'next/link'

export default function Home({ data }) {
  let [currentPage, setCurrentPage] = useState(1)

  let postsPerPage = 10;
  let indexOfLastPost = postsPerPage * currentPage;
  let indexOfFirsPost = indexOfLastPost - postsPerPage;

  let currentArray = data.slice(indexOfFirsPost, indexOfLastPost);

  return (
    <div className={styles.container}>
      <Head>
        <title>Alkemy Blog</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <main className="p-4">


        <div className="flex flex-col justify-center max-w-2xl">

          {currentArray.map(post =>
            <Link href={`/post/${post.id}`}>
              <div key={post.id} className="border m-3 cursor-pointer">
                <div className="p-5">
                  <h2 className="text-xl font-semibold">{post.title}</h2>
                </div>
              </div>
            </Link>
          )}

        </div>
        <div className="flex justify-between items-center">
          <div>
            <button className="p-2 bg-indigo-300 mr-3" onClick={() => setCurrentPage(currentPage - 1)}>back</button>
            <button className="p-2 bg-indigo-300" onClick={() => setCurrentPage(currentPage + 1)}>next</button>
          </div>

          <div>
            {`${indexOfLastPost}/${data.length}`}
          </div>
        </div>

      </main>

      <Footer />
    </div>
  )
}

export async function getStaticProps(context) {
  const res = await fetch(`https://jsonplaceholder.typicode.com/posts`)
  const data = await res.json()

  return {
    props: { data }, // will be passed to the page component as props
  }
}