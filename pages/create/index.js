import Head from 'next/head'
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';

import styles from '../../styles/Home.module.css'

export default function Home({ }) {

    return (
        <div className={styles.container}>
            <Head>
                <title>Alkemy Blog</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar />
            <main className={styles.main}>


                <div className="flex flex-col justify-center w-full">
                    <form className="flex flex-col" action="">
                        <div className="pt-2 pb-4 w-full">
                            <label className="block text-xl font-medium text-gray-800 pb-5" htmlFor="title">Title</label>
                            <input type="text" name="title" id="title" className=" focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300" placeholder="Title" />
                        </div>


                        <div className="mt-1 mb-3">
                            <label className="block text-xl font-medium text-gray-800 pb-5" htmlFor="title">sunt aut facere repellat provident occaecati excepturi optio reprehenderit</label>
                            <textarea id="about" name="about" rows="3" className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="Write here..."></textarea>
                        </div>
                        <button className="bg-blue-400 text-white p-3 border-radius-2">Submit</button>

                    </form>
                </div>

            </main>

            <Footer />
        </div>
    )
}